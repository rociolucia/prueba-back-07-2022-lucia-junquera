<?php

namespace App\Http\Controllers\Api\Company;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Vocces\Company\Application\CompanyList;
use App\Http\Requests\Company\ListCompanyRequest;

class GetListCompanyController extends Controller
{
    /**
     * List new company
     *
     * @param \App\Http\Requests\Company\ListCompanyRequest $request
     */
    public function __invoke(ListCompanyRequest $request, CompanyList $service)
    {
        DB::beginTransaction();
        try {
            $companies = $service->handle();
            DB::commit();
            return response($companies, 201);
        } catch (\Throwable $error) {
            DB::rollback();
            throw $error;
        }
    }
}