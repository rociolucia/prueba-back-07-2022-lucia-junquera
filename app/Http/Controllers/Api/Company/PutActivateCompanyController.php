<?php

namespace App\Http\Controllers\Api\Company;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Vocces\Company\Application\CompanyActivator;
use App\Http\Requests\Company\ActivateCompanyRequest;

class PutActivateCompanyController extends Controller
{
    /**
     * Create new company
     *
     * @param \App\Http\Requests\Company\ActivateCompanyRequest $request
     */
    public function __invoke(ActivateCompanyRequest $request, CompanyActivator $service)
    {
        DB::beginTransaction();
        try {
            $company = $service->handle($request->idCompany);
            DB::commit();
            return response("Activated", 201);
        } catch (\Throwable $error) {
            DB::rollback();
            throw $error;
        }
    }
}
