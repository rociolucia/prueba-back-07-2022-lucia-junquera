<?php

namespace Vocces\Company\Domain;

use Illuminate\Database\Eloquent\Collection;

interface CompanyRepositoryInterface
{
    /**
     * Persist a new company instance
     *
     * @param Company $company
     *
     * @return void
     */
    public function create(Company $company): void;

    public function activate($idCompany): void;

    public function list(): Collection;
}
