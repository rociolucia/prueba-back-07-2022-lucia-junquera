<?php

namespace Vocces\Company\Infrastructure;

use Illuminate\Database\Eloquent\Collection;
use App\Models\Company as ModelsCompany;
use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\CompanyRepositoryInterface;
use Vocces\Company\Domain\ValueObject\CompanyStatus;
use Vocces\Company\Domain\ValueObject\CompanyList;

class CompanyRepositoryEloquent implements CompanyRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(Company $company): void
    {
        ModelsCompany::Create([
            'id'     => $company->id(),
            'name'   => $company->name(),
            'status' => $company->status(),
        ]);
    }

    public function activate($idCompany): void 
    {
        $company = ModelsCompany::Find($idCompany);

        $company->status = CompanyStatus::ENABLED;

        $company->save();
    }

    public function list(): Collection
    {
        $companies = ModelsCompany::ALL();

        return $companies;
    
    }
}
