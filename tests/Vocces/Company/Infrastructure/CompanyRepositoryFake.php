<?php

namespace Tests\Vocces\Company\Infrastructure;

use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\CompanyRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class CompanyRepositoryFake implements CompanyRepositoryInterface
{
    public bool $callMethodCreate = false;
    public bool $callMethodActivate = false;
    public bool $callMethodList = false;

    /**
     * @inheritdoc
     */
    public function create(Company $company): void
    {
        $this->callMethodCreate = true;
    }
    
    public function activate($idCompany): void
    {
        $this->callMethodActivate = true;
    }

    public function list(): Collection
    {
        $this->callMethodList = true;

        return [];
    }
}

